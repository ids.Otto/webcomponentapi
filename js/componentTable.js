class RenglonTabla extends HTMLElement {
	constructor(email, nombre, apellido, id, resultadoBusqueda) {
		super();
		this.template = document.getElementById('renglon');
		this.miTabla = document.getElementById('tBody');
		this.email = email;
		this.nombre = nombre;
		this.apellido = apellido;
		this.id = id;
		this.arreglo = [];
		this.btnApi = document.querySelector('#btnAPI');
		/* this.btnClear = document.querySelector('.btnClear'); */
		this.resultadoBusqueda = resultadoBusqueda;
		this.busqueda = false;
		this.error = false;
	}

	connectedCallback() {
		getAPI(this.arreglo);
		let cloneNode = document.importNode(
			this.template.content,
			true,
		);
		this.miTabla.appendChild(cloneNode);
		this.btnApi.addEventListener('click', this.getDatos());

		this.filterDato();
		this.clearFilter();
	}

	getDatos() {
		this.btnApi.addEventListener('click', () =>
			recorreDatos(
				this.arreglo,
				this.id,
				this.nombre,
				this.apellido,
				this.email,
				this.miTabla,
				this.template.content,
			),
		);
	}

	getDato() {
		this.busqueda = true;
		let cloneNode = document.importNode(
			this.template.content,
			true,
		);
		this.arreglo.forEach(dato => {
			try {
				if (
					this.resultadoBusqueda.toLowerCase() ===
					dato.first_name.toLowerCase()
				) {
					let hijos = document.querySelectorAll('tr');
					for (let i = 1; i < hijos.length; i++) {
						hijos[
							i
						].childNodes[3].textContent.toLowerCase() ===
						this.resultadoBusqueda.toLowerCase()
							? (this.error = false)
							: hijos[i].remove();
					}
				}
			} catch (e) {
				this.error = true;
			}
		});
		if (this.error) {
			const miError = new MuestraError(this.error);
			miError.mostrar();
		}
		this.miTabla.appendChild(cloneNode);
	}

	filterDato() {
		const btnSearch = document.querySelector('.btnSearch');
		let txtSearch = document.querySelector('#txtSearch');

		btnSearch.addEventListener('click', () => {
			if (txtSearch.value !== '') {
				const nombres =
					document.querySelectorAll('.nombreDatos');
				nombres.forEach(nombre =>
					txtSearch.value.toLowerCase() ===
					nombre.textContent.toLowerCase()
						? (this.resultadoBusqueda = nombre.textContent)
						: null,
				);
				this.getDato();
			}
		});
	}

	clearFilter() {
		const btnClear = document.querySelector('.btnClear');
		let txtSearch = document.querySelector('#txtSearch');

		btnClear.addEventListener('click', () => {
			txtSearch.value = '';

			let hijos = document.querySelectorAll('tr');
			for (let i = 1; i < hijos.length; i++) {
				hijos[i].remove();
			}
			this.busqueda = false;
			recorreDatos(
				this.arreglo,
				this.id,
				this.nombre,
				this.apellido,
				this.email,
				this.miTabla,
				this.template.content,
			);
		});
	}
}

window.customElements.define('renglon-tabla', RenglonTabla);

/* ------------------------------------------------------------- */
/* ------------------------------------------------------------- */
/* FIN DE LA CLASE */
/* ------------------------------------------------------------- */
/* ------------------------------------------------------------- */

/* Función para obtener la api */
function getAPI(arreglo) {
	const api = 'https://reqres.in/api/users';

	fetch(api)
		.then(res => res.json())
		.then(data =>
			data.data.forEach(dato => arreglo.push(dato)),
		);
}

/* funcion que agrega un atributo a mis elementos de mi template */
const agregarAtr = (arreglo, valor) => {
	arreglo.forEach(td => {
		if (!td.hasAttribute('mostrado')) {
			td.textContent = valor;
			td.setAttribute('mostrado', true);
		}
	});
};

/* función que hace un querySelectorAll */
const buscarClase = clase =>
	document.querySelectorAll(`.${clase}`);

/* función para recorrer mis datos */
const recorreDatos = (
	objeto,
	id,
	nombre,
	apellido,
	email,
	tabla,
	template,
) => {
	for (let [llave, valor] of Object.entries(objeto)) {
		let cloneNode = document.importNode(template, true);

		let tdIds = buscarClase('idDatos');
		let tdNombreDatos = buscarClase('nombreDatos');
		let tdApellidoDatos = buscarClase('apellidoDatos');
		let tdEmail = buscarClase('emailDatos');

		email = valor.email;
		nombre = valor.first_name;
		apellido = valor.last_name;
		id = valor.id;

		agregarAtr(tdIds, id);
		agregarAtr(tdNombreDatos, nombre);
		agregarAtr(tdApellidoDatos, apellido);
		agregarAtr(tdEmail, email);

		tabla.appendChild(cloneNode);
	}
};

const buscaDato = (
	value,
	objeto,
	id,
	nombre,
	apellido,
	email,
	tabla,
	template,
) => {
	let hijos = document.querySelectorAll('td');
	hijos.forEach(hijo => hijo.remove());
	for (let [llave, valor] of Object.entries(objeto)) {
		let cloneNode = document.importNode(template, true);

		let tdNombreDatos = buscarClase('nombreDatos');

		/* nombre = valor.first_name; */
		/* console.log(valor.first_name); */
		console.log(tabla.children);

		if (
			value.toLowerCase() === valor.first_name.toLowerCase()
		) {
			nombre = value;
			console.log('encontrado ' + value);
			console.log(cloneNode);
			return;
		}

		tabla.appendChild(cloneNode);
	}
};
