window.addEventListener('load', () => {
	const api = 'https://reqres.in/api/users';
	const url =
		'http://dummy.restapiexample.com/api/v1/employees';

	const btnAPI = document.querySelector('#btnAPI');
	const tBody = document.querySelector('.tbody');

	const getAPI = () => {
		/* fetch(api)
			.then(res => res.json())
			.then(json => {
				console.log(json.data);
				json.data.forEach(persona => {
					const trTabla = document.createElement('tr');
					const tdId = document.createElement('td');
					const tdNombre = document.createElement('td');
					const tdApellido = document.createElement('td');
					const tdEmail = document.createElement('td');
					tdId.textContent = persona.id;
					tdNombre.textContent = persona.first_name;
					tdApellido.textContent = persona.last_name;
					tdEmail.textContent = persona.email;

					tBody.appendChild(trTabla);
					trTabla.appendChild(tdId);
					trTabla.appendChild(tdNombre);
					trTabla.appendChild(tdApellido);
					trTabla.appendChild(tdEmail);
				});
			}); */

		fetch(api)
			.then(res => res.json())
			.then(json => {
				/* console.log(json.data); */
				let contador = 0;
				for (let [llave, valor] of Object.entries(
					json.data,
				)) {
					/* console.log(`${llave}: ${valor.email}`); */
					/* console.log(valor.first_name); */
					crearFila(valor);
				}

				/* json.data.forEach(persona => {
					crearFila(persona);
				}); */
				/* const tdId = document.querySelectorAll('.idData');
				console.log(tdId); */
			});
	};

	function crearFila(objeto) {
		const tBody = document.querySelector('.tbody');
		const tpl = document.querySelector('template');
		const tplInst =
			tpl.content.cloneNode(true); /* tr #trFila */
		const tdsDatos = document.querySelectorAll('.datos');
		tBody.appendChild(tplInst);
		crearElemento(objeto.id);
		/* console.log(tpl.children); */

		/* tdTabla.textContent = objeto; */
		/* console.log(objeto.id); */
		/* crearElemento(objeto); */

		/* tdId.textContent = 'hhh'; */
		/* console.log(objeto.first_name); */
	}

	function crearElemento(valor) {
		const trFila = document.querySelectorAll('tr');

		/* const tpl = document.querySelector('#tplTd');
		const tplInst = tpl.content.cloneNode(true);
		console.log(typeof tplInst); */
		/* console.log(trFila); */
	}

	btnAPI.addEventListener('click', getAPI);
});

/* class MiElemento extends HTMLElement {
	constructor() {
		const tpl = document.querySelector('#tplTd');
		const tplInst = tpl.content.cloneNode(true);
		const trPersona = document.querySelector('.trFila');
		super();

		this.attachShadow({ mode: 'open' });

		this.shadowRoot.appendChild(tplInst);
		tBody.appendChild(tplInst);
	}
}

customElements.define('mi-elemento', MiElemento, {
	extends: 'td',
}); */

function prueba() {
	/* let mycute = {
		name: 'angel',
		age: 3,
		isjk: true,
		likes: [],
		fur: { color: ['orange', 'white'], pattern: 'stripped' },
		meou: function () {},
	};
	console.log(mycute.fur[1]); */
	console.log(typeof null);
}

prueba();
