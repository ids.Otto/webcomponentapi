class MuestraError extends HTMLElement {
	constructor(existeError) {
		super();
		this.template = document.getElementById('error');
		this.containerTable = document.querySelector(
			'.containerTable',
		);
		this.existeError = existeError;
		this.nodoClonado = document.importNode(
			this.template.content,
			true,
		);
	}
	mostrar() {
		if (this.existeError) {
			let txtSearch = document.querySelector('#txtSearch');
			txtSearch.value = '';
			this.containerTable.insertBefore(
				this.nodoClonado,
				this.containerTable[0],
			);
			setTimeout(() => {
				const containerError = document.querySelector(
					'.containerError',
				);
				console.log(containerError);
				containerError.remove();
			}, 3000);
		}
	}
}

window.customElements.define('componente-error', MuestraError);
